﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjInterdisciplinar
{
	class Program
	{
		static void Main(string[] args)
		{
			int controle = 1;

			while (controle != 0)
			{
				Console.Write("Informe a quantidade de vertices: ");
				controle = Convert.ToInt32(Console.ReadLine ());
				Console.Write ("Direcionado? (1) para sim (0) para nao: ");
				bool direcionado = Convert.ToBoolean(Convert.ToInt32 (Console.ReadLine ()));

				Grafo grafo = new Grafo (controle, direcionado);
				grafo.GerarGrafoPadrao ();
				//grafo.MostraMatriz ();

				grafo.gerarListaAdj ();
				Console.WriteLine ();

				Console.Write ("Origem: ");
				int orig = Convert.ToInt32 (Console.ReadLine ());

				Console.Write ("Destino: ");
				int dest = Convert.ToInt32 (Console.ReadLine ());

				int distanciaMin = grafo.RunDijkstra (orig, dest);
				int distanciaMinB = grafo.RunBellmanFord (orig, dest);


				Console.WriteLine ("Dijkstra Distancia Minima = " + distanciaMin);
				Console.WriteLine ("BellmanFord Distancia Minima = " + distanciaMinB);

				Console.WriteLine ("Pressione uma tecla para continuar.");
				Console.ReadKey ();

				Console.Clear ();
			}
		}
	}
}
