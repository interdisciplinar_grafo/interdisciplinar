using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjInterdisciplinar
{

	class Aresta
	{
		public Vertice vert;
		public int peso;
	}

	class Vertice
	{
		public int valor; //valor do vertice
		public bool visitado; //visitado ou nao
		public List<Aresta> arestas;

		public Vertice()
		{
			arestas = new List<Aresta>();
			visitado = false;
		}
	}

	class Grafo
	{
		List<Vertice> listAdj;
		int nVerts;
		bool direcionado;
		int[,] matrizAdj;
		int[] conexov;

		public Grafo(int nVerts, bool direcionado)
		{
			this.nVerts = nVerts;
			this.direcionado = direcionado;
			matrizAdj = new int[nVerts, nVerts];
			listAdj = new List<Vertice>();	
			conexov = new int[nVerts];
		}

		public void GerarGrafoPadrao()
		{
			Random rand = new Random ();

			//Gerar matriz
			for (int i = 0; i < nVerts; i++) 
			{
				for (int j = 0; j < nVerts; j++)
				{
					if (i == j) 
					{
						matrizAdj [i, j] = 0;
					} 
					else 
					{
						if (direcionado) {
							int ind = rand.Next (0, 20);
							int aux = rand.Next (0, 2);

							if (aux == 0) {
								matrizAdj [j, i] = ind;
								matrizAdj [i, j] = 0;
							} else {
								matrizAdj [j, i] = 0;
								matrizAdj [i, j] = ind;
							}
						} 
						else {
							int ind = rand.Next (0, 20);
							matrizAdj [i, j] = ind;
							matrizAdj [j, i] = ind;
						}
					}
				}
			}
		}

		public void MostraMatriz()
		{
			//Mostra matriz
			for (int i = 0; i < nVerts; i++)
			{
				for (int j = 0; j < nVerts; j++)
				{
					Console.Write(" " + matrizAdj[i, j]);
				}
				Console.WriteLine();
			}
		}
		public void gerarListaAdj()
		{
			listAdj = new List<Vertice>();

			//Apenas inicializa os valores de cada vertice e os cria.
			for(int i = 0; i < nVerts; i++)
			{
				listAdj.Add(CriaVertice());
				for (int j = 0; j < nVerts; j++)
				{
					listAdj[i].valor = i;
				}
			}
			for (int i = 0; i < nVerts; i++)
			{
				for (int j = 0; j < nVerts; j++)
				{
					if(matrizAdj[i, j] > 0)
					{
						Aresta relacao = new Aresta();
						relacao.vert = listAdj[j];
						relacao.peso = matrizAdj[i, j];

						listAdj[i].arestas.Add(relacao);
					}

				}
			}
		}

		private Vertice CriaVertice()
		{
			Vertice newVertice = new Vertice();
			return newVertice;
		}

		//Algoritmo de Dijkstra
		private int Dijkstra(int orig, int dest)
		{
			int[] dist = new int[nVerts]; // Vetor de distancias
			bool[] visitados = new bool[nVerts]; //Vetor de visitados, serve para caso o vertice ja tenha sido expandido, não expandir mais;
			priority_queue filaPrio = new priority_queue();

			for(int i = 0; i < nVerts; i++)
			{
				dist[i] = int.MaxValue;
				visitados[i] = false;
			}

			//A distancia de orig para orig é 0
			dist[orig] = 0;

			//Insere na fila
			filaPrio.push(dist[orig], orig);

			//loop principal do algoritmo
			while(!filaPrio.empty())
			{
				Tuple<int, int> p = filaPrio.min(); //obtem o valor minimo da fila de prioridade
				int u = p.Item2; //Obtém o vértice da tupla

				//Verifica se o vértice não foi expandido
				if(visitados[u] == false)
				{
					//Marca como visitado
					visitados[u] = true;

					//Percorre os vértices adjacentes de "u"
					for(int i = 0; i < listAdj[u].arestas.Count; i++)
					{
						//Obtem o vértice adjacente e o custo para se chegar nele
						int v = listAdj[u].arestas[i].vert.valor;
						int custo = listAdj[u].arestas[i].peso;

						//relaxamento (u, v)
						if(dist[v] > (dist[u] + custo))
						{
							//Atualiza a distancia de v e insere na fila
							dist[v] = dist[u] + custo;
							filaPrio.push(dist[v], v);
						}
					}
				}
			}

			return dist[dest];
		}

		private int BellmanFord(int orig, int dest)
		{
			int[] dist = new int[nVerts];
			int[] parent = new int[nVerts];

			//Inicialização
			for (int i = 0; i < nVerts; i++) 
			{
				dist [i] = 999;
				parent [i] = -1;
			}

			dist [orig] = 0;


			//Relaxamento das arestas
			for (int i = 0; i < nVerts - 1; i++) 
			{
				for(int j = 0; j < nVerts; j++)
				{
					//Bellman Ford com lista de adjacencia
					for (int k = 0; k < listAdj [j].arestas.Count; k++) {
						int u = listAdj [j].valor;
						int v = listAdj [j].arestas [k].vert.valor;
						int custo = listAdj [j].arestas [k].peso;

						if (dist [v] > dist [u] + custo) {
							dist [v] = dist [u] + custo;
						}
					}
				}
			}

			//Verificação de ciclos negativos
			for (int j = 0; j < nVerts; j++) {
				for (int k = 0; k < nVerts; k++) {
					if (matrizAdj [j, k] > 0) {
						if (dist [k] > dist [j] + matrizAdj [j, k]) {
							return -1;
						}
					}
				}
			}
			return dist [dest];
		}
		public int RunDijkstra(int orig, int dest)
		{
			Stopwatch sw = new Stopwatch ();
			sw.Start ();
			int custoMinimo = Dijkstra (orig, dest);
			sw.Stop ();
			TimeSpan time = sw.Elapsed;

			Console.ForegroundColor = ConsoleColor.White;
			Console.Write ("Algoritmo Dijkstra executou em "); 
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine(time.TotalMilliseconds + " ms");
			Console.ForegroundColor = ConsoleColor.White;

			return custoMinimo;
		}
		public int RunBellmanFord(int orig, int dest)
		{
			Stopwatch sw = new Stopwatch ();
			sw.Start ();
			int custoMinimo = BellmanFord (orig, dest);
			sw.Stop ();
			TimeSpan time = sw.Elapsed;

			Console.ForegroundColor = ConsoleColor.White;
			Console.Write ("Algoritmo Bellman Ford executou em "); 
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine(time.TotalMilliseconds + " ms");
			Console.ForegroundColor = ConsoleColor.White;

			return custoMinimo;
		}

		public void ImprimePesos()
		{
			for(int i = 0; i < listAdj.Count; i++)
			{
				for (int j = 0; j < listAdj[i].arestas.Count; j++)
				{
					Console.WriteLine("" + listAdj[i].valor + ": " + "Destino => " + listAdj[i].arestas[j].vert.valor
						+ " peso = " + listAdj[i].arestas[j].peso);
				}
			}
		}

		public void ImprimeLista()
		{
			for (int i = 0; i < listAdj.Count; i++)
			{
				Console.Write("{" + listAdj[i].valor + "} =>  ");
				for (int j = 0; j < listAdj[i].arestas.Count; j++)
				{
					if (j >= listAdj[i].arestas.Count - 1)
						Console.WriteLine(" " + listAdj[i].arestas[j].vert.valor + " ");

					else
						Console.Write(" " + listAdj[i].arestas[j].vert.valor + ",");
				}
			}
		}
	}
}
