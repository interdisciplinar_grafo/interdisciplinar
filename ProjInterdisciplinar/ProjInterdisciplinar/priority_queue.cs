﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjInterdisciplinar
{
    class priority_queue
    {   
		//item1 = distancia
		//item2 = valor do vertice
        private LinkedList<Tuple<int, int>> data;

        public priority_queue()
        {
            data = new LinkedList<Tuple<int, int>>();
        }

        public void push(int dist, int v)
        {
            data.AddLast(Tuple.Create<int, int>(dist, v));
        }

        public Tuple<int, int> min()
        {
            var tmp = data.Min();
            pop();
            return tmp;
        }

        public void pop()
        {
            data.Remove(data.Min());
        }

        public bool empty()
        {
            return data.Count == 0;
        }
    }
}
